'use strict';

var app = angular.module("tableApp", []);

app.factory("TableService", function($q){

	var data = {};

    var promiseSimulation = $q(function(resolve, reject){
        resolve(tableData); 
    })

    /**
     * This is fragile because we need to store the desired categories.
     * I am going to pull them from one of the responses.
     * Ideally, I would like to backend to return this to me because I cannot
     * really trust that a sample response has all the categories I need.
     */
    function generateCategories(table){
        var cycleCategories = Object.keys(table.crop_products["Commercial Corn"].production_cycles[2015]);
        return cycleCategories;
    }

    /**
     * Get the list of production cycles from the data for filtering.
     * Default value included in list.
     */
    function generateProductionCycles(table){
        return _.uniq(_.reduce(table.crop_products, function(result, value, key){
            _.reduce(value.production_cycles, function(result, value, key){
                result.push(key);
                return result;
            }, result);
            return result;
        },['Production Cycle']));
    }

    /**
     * Get the list of crop products from the data for filtering.
     * Default value included in list.
     */
    function generateCropProducts(table){
        return _.uniq(_.reduce(table.crop_products, function(result, value, key){
            result.push(key);
            return result;
        },['Crop Products']));
    }

	function getModel(){
        promiseSimulation.then(function(response){
            data.table = response;
            data.categories = generateCategories(data.table);
            data.production_cycles = generateProductionCycles(data.table);
            data.crop_products = generateCropProducts(data.table);
        }).catch(function(reason){
            // There is no possible rejection in this simulation.
        });
		return data;
	}

	return {
		getModel:getModel
	}
});

app.controller("MainController", function($scope, TableService, $timeout){

    /**
     * We'll setup the page's model binding here first.
     * Also we have my array that will hold the column sizes for the child tables.
     */
    $scope.tableModel = TableService.getModel();
    $scope.headerSizes = [];

    /**
     * I want to simulate async waiting behavior from the backend here.
     * We'll wait for the model to be updated by the service
     * and not block the page from renering while the call is fulfilled.
     */
    $scope.$watch('tableModel', function(newValue, oldValue){
        $scope.selectedCycle = newValue.production_cycles[0];
        $scope.selectedProduct = newValue.crop_products[0];
    }, true);

    $scope.resetSelections = function(){
        $scope.selectedCycle = $scope.tableModel.production_cycles[0];
        $scope.selectedProduct = $scope.tableModel.crop_products[0];
    }

})
app.directive('subTable', function(){
    return {
        scope:{
            table:'=',
            selectedCycle:'=',
            availableCycles:'=',
            headerSizes:'=',
            title:'='
        },
        restrict:'A',
        replace:true,
        template:   '<div>' +
            '{{title}}&nbsp;' +
            '<i ng-show="!displayTable" class="fa fa-caret-down" aria-hidden="true" ng-click="displayTable = !displayTable"></i>' +
            '<i ng-show="displayTable" class="fa fa-caret-up" aria-hidden="true" ng-click="displayTable = !displayTable"></i>' +
            '<table ng-show="displayTable" style="border-collapse:collapse;">' +
            '<tr ng-repeat="(year, data) in table" ng-if="year === selectedCycle || selectedCycle === availableCycles[0]">' +
            '<td ng-repeat="(key, value) in data" style="width:{{headerSizes[$index]}}px">' +
            '{{formatValue(value)}}' +
            '</td>' +
            '</tr>' +
            '</table>' +
            '</div>',
        controller:function($scope){
            $scope.displayTable = true;
            $scope.formatValue = function(value){
                if(value === "Undefined" || value === "NaN" || value === ""  || value === null || value === undefined){
                    // return "[Data Unavaliable]";
                    return "--";
                }else{
                    return value;
                }
            }
        },
        link:function(scope, element, attributes){}
    }
})
app.directive('categories', function($timeout, $window){
    return {
        restrict:"A",
        replace:true,
        scope:{
            list:'=',
            sizes:'='
        },
        template:"<th ng-repeat='item in list' style='' class='header-item'>{{item}}</th>",
        controller: function($scope){
            // console.log('directive loaded', $scope.list)
        },
        link: function(scope, element, attributes){
            var headerItems = document.getElementsByClassName("header-item");
            // have to do this after compilation, push it on to the event loop.
            $timeout(function(){
                angular.forEach(headerItems, function(item, index){
                    /**
                     * NOTE: Magic Number. There are some things with tables and spacing and client widths
                     * That are just too annoying to deal with for a test...
                     */
                    scope.sizes.push(item.clientWidth-2);
                })    
            },0);
        }
    }
})

var tableData = {
    "org_name": "ABC Farms",
    "crop_products": {
        "Commercial Corn": {
            "production_cycles": {
                "2015": {
                    "Production Cycle": 2015,
                    "Total Production": "12345.67 bu",
                    "Futures Month": "May 16",
                    "Current Futures": "$1.25",
                    "Priced Futures": "$5.67",
                    "Final Futures": ""
                },
                "2016": {
                    "Production Cycle": 2016,
                    "Total Production": "67589.67 bu",
                    "Futures Month": "Apr 16",
                    "Current Futures": "",
                    "Priced Futures": "$3.87",
                    "Final Futures": "$1.23"
                }
            }
        },
        "Seed Corn": {
            "production_cycles": {
                "2015": {
                    "Production Cycle": 2015,
                    "Total Production": "23109.75 bu",
                    "Futures Month": "May 16",
                    "Current Futures": "$3.75",
                    "Priced Futures": "Undefined",
                    "Final Futures": "$6.99"
                },
                "2016": {
                    "Production Cycle": 2016,
                    "Total Production": "8639283.98 bu",
                    "Futures Month": "Apr 16",
                    "Current Futures": "",
                    "Priced Futures": "$2.88",
                    "Final Futures": "$2.43"
                }
            }
        },
        "Commercial Soybean": {
            "production_cycles": {
                "2015": {
                    "Production Cycle": 2015,
                    "Total Production": "987923.23 bu",
                    "Futures Month": "May 16",
                    "Current Futures": "$5.59",
                    "Priced Futures": "NaN",
                    "Final Futures": "$4.94"
                },
                "2016": {
                    "Production Cycle": 2016,
                    "Total Production": "8639283.98 bu",
                    "Futures Month": "Apr 16",
                    "Current Futures": "$3.99",
                    "Priced Futures": "$8.43",
                    "Final Futures": null
                }
            }
        }
    }
};